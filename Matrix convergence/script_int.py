import random
from multiprocessing import Pool
import time

import time
def matmult(a,b):
    zip_b = zip(*b)
    zip_b = list(zip_b)
    return [[sum(ele_a*ele_b for ele_a, ele_b in zip(row_a, col_b))
             for col_b in zip_b] for row_a in a]




#print("\n enter line_a value")
line_a = int(1000)
#print("\n enter column_a value")
column_a = int(1000)  # кол-во строк и столбцов в первой матрице
#print("\n enter line_b value")
line_b = int(1000)
#print("\n enter column_b value")
column_b = int(1000)# кол-во строк и столбцов во второй матрице
A = [[random.randint(0, 100) for l in range(column_a)] for l in range(line_a)]#random.uniform() выдаёт вещественные числа в заданном промежутке(данный скрипт отличается от алгоритма с целочисленными числами только словом uniform())
B = [[random.randint(0, 100) for q in range(column_b)] for q in range(line_b)]

#print("Матрица А:")
#print(index(A))

#print("Матирца В")
#print(index(B))
f = open("matrix_convergence_multiprocess.txt", "a+")
start = time.time()
if __name__ == '__main__':
     multproc = Pool(processes=7)
     prs1 = multproc.apply_async(matmult(A, B))
     prs2 = multproc.apply_async(matmult(A, B))
     prs3 = multproc.apply_async(matmult(A, B))
     prs4 = multproc.apply_async(matmult(A, B))
     prs5 = multproc.apply_async(matmult(A, B))
     prs6 = multproc.apply_async(matmult(A, B))
     prs7 = multproc.apply_async(matmult(A, B))
multproc.close()
multproc.join()
end = time.time() - start
f.write(str('{:.6f}'.format(end)) + ';' + '\n')
print("untill it done")
